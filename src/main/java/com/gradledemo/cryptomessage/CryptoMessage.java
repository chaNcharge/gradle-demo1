package com.gradledemo.cryptomessage;

/**
 * Challenge
 * A class that encrypts and decrypts strings
 * @author Ethan Cha
 */
public class CryptoMessage {

    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public static void main(String[] args) {
        UserInput ui = new UserInput();
        CryptoMessage self = new CryptoMessage();
        String choice = ui.readLine("Encrypt or decrypt? ");
        String message = ui.readLine("Message: ");
        int cipher = ui.readInt("Cipher: ");
        switch (choice) { // Another way of saying if choice == "encrypt" or choice == "decrypt", it runs a function
            case "encrypt":
                System.out.println(self.encrypt(message, cipher));
                break;
            case "decrypt":
                System.out.println(self.decrypt(message, cipher));
                break;
            default: // Default choice if none of the other cases match
                System.out.println("Invalid option of encrypting or decrypting.");
                break;
        }
    }

    /**
     * Encrypts a message using given text and key.
     * @param rawtext The text to be encrypted.
     * @param key The Caesar's cipher key to use
     *            for encrypting text.
     */
    public String encrypt(String rawtext, int key) {
        String text = rawtext.toLowerCase();
        char[] encryptedChars = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == ' ') { // Add space to array, if there is a space
                encryptedChars[i] = ' ';
            } else if (ALPHABET.indexOf(c) >= 0) { // Check if letter is in alphabet
                int position = ALPHABET.indexOf(c);
                int newPosition = ((position + key) % 26);
                char newC = ALPHABET.charAt(newPosition);
                encryptedChars[i] = newC;
            } else { // Otherwise add without encrypting
                encryptedChars[i] = c;
            }
        }
        return new String(encryptedChars);
    }

    /**
     * Decrypts a message using given text and key.
     * @param rawtext The text to be decrypted.
     * @param key The Caesar's cipher key to use
     *            for decrypting text.
     */
    public String decrypt(String rawtext, int key) {
        String text = rawtext.toLowerCase();
        char[] decryptedChars = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == ' ') { // Add space to array, if there is a space
                decryptedChars[i] = ' ';
            } else if (ALPHABET.indexOf(c) >= 0) { // Check if letter is in alphabet
                int position = ALPHABET.indexOf(c);
                int newPosition = ((position - key) % 26);
                char newC = ALPHABET.charAt(newPosition);
                decryptedChars[i] = newC;
            } else { // Otherwise add without decrypting
                decryptedChars[i] = c;
            }
        }
        return new String(decryptedChars);
    }
}

