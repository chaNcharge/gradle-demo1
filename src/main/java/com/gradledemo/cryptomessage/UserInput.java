package com.gradledemo.cryptomessage;

import java.util.Scanner;

/**
 * This class consists of functions to be used alongside other
 * classes that use user input. You can create these yourself in
 * a single class, since it's fairly simple but this makes
 * things easier, as it is aligned with CodeHS. This is also very
 * similar to how Python's input() works.
 * @author Ethan Cha
 */

public class UserInput {
    Scanner s = new Scanner(System.in);

    public String readLine(String prompt) {
        System.out.print(prompt);
        return s.nextLine();
    }

    public int readInt(String prompt) {
        System.out.print(prompt);
        return s.nextInt();
    }

    public double readDouble(String prompt) {
        System.out.print(prompt);
        return s.nextDouble();
    }

    public boolean readBoolean(String prompt) {
        System.out.print(prompt);
        return s.nextBoolean();
    }
}

